/* Copyright 2019 core2idiot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "quantum.h"

/* This is a shortcut to help you visually see your layout.
 *
 *          ---Layout in switch numbers---
 * |*20*|*17*|*18*|*19*|*14*|*15*|*16*|*11*|*12*|*13*|* 5*|
 * |*51*|*21*|*22*|*23*|*24*|*25*|*26*|*27*|*28*|*29*|*30*|
 * |*57*|*31*|*32*|*33*|*34*|*35*|*36*|*37*|*38*|*39*|*50*|
 * |*41*|*42*|*43*|*44*|*45*|*46*|*47*|*48*|*58*|*59*|* 6*|
 * |* 9*|* 8*|* 7*|*10*|*55*|    |*56*|*53*|*49*|*40*|*60*|
 * |*52*|*54*|                                  |* 2*|
 *                                         |* 1*|    |* 4*|
 *                                              |* 3*|
 *          ---Layout in Rows/Cols---
 * |*19*|*16*|*17*|*18*|*13*|*14*|*15*|*10*|*11*|*12*|*04*|
 * |*50*|*20*|*21*|*22*|*23*|*24*|*25*|*26*|*27*|*28*|*29*|
 * |*56*|*30*|*31*|*32*|*33*|*34*|*35*|*36*|*37*|*38*|*49*|
 * |*40*|*41*|*42*|*43*|*44*|*45*|*46*|*47*|*57*|*58*|*05*|
 * |*08*|*07*|*06*|*09*|*54*|    |*55*|*52*|*48*|*39*|*59*|
 * |*51*|*53*|                                  |*01*|
 *                                         |*00*|    |*03*|
 *                                              |*02*|
 * The first section contains all of the arguments representing the physical
 * layout of the board and position of the keys.
 *
 * The second converts the arguments into a two-dimensional array which
 * represents the switch matrix.
 */




//K19-04 Correct
// Gp0///
//  - r shift l -
// GGG
#define LAYOUT( \
    k19, k16, k17, k18, k13, k14, k15, k10, k11, k12, k04,\
    k50, k30, k31, k32, k33, k34, k35, k36, k37, k38, k39,\
    k56, k40, k41, k42, k43, k44, k45, k46, k47, k48, k29,\
    k20, k21, k22, k23, k24, k25, k26, k27, k57, k58, k05,\
    k08, k07, k06, k09, k54,      k55, k52, k28, k49, k59,\
    k51, k53,                                    k01,     \
                                            k00,      k03,\
                                                 k02\
) \
{ \
    {k00, k01, k02, k03, k04, k05, k06, k07, k08, k09}, \
    {k10, k11, k12, k13, k14, k15, k16, k17, k18, k19}, \
    {k20, k21, k22, k23, k24, k25, k26, k27, k28, k29}, \
    {k30, k31, k32, k33, k34, k35, k36, k37, k38, k39}, \
    {k40, k41, k42, k43, k44, k45, k46, k47, k48, k49},  \
    {k50, k51, k52, k53, k54, k55, k56, k57, k58, k59},  \
}
