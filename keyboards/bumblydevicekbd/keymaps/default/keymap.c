/* Copyright 2019 core2idiot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include QMK_KEYBOARD_H

// Defines names for use in layer keycodes and the keymap
enum layer_names {
    _BASE,
    _FN
};

// Defines the keycodes used by our macros in process_record_user
enum custom_keycodes {
    QMKBEST = SAFE_RANGE,
    QMKURL
};
  /*k19, k16, k17, k18, k13, k14, k15, k10, k11, k12, k04,\
    k50, k20, k21, k22, k23, k24, k25, k26, k27, k28, k29,\
    k56, k30, k31, k32, k33, k34, k35, k36, k37, k38, k49,\
    k40, k41, k42, k43, k44, k45, k46, k47, k57, k58, k05,\
    k08, k07, k06, k09, k54,      k55, k52, k48, k39, k59,\
    k51, k53,                                    k01,     \
                                            k00,      k03,\
                                                 k02\*/
/*
 * |*ESC  *|*   1*|*2*|*3*|*4  *|*5 *|* 6 *|*7 *|*   8*|*9 *|*  0*|
 * |*TAB  *|*   Q*|*W*|*E*|*R  *|*T *|* Y *|*U *|*   I*|*O *|*  P*|
 * |*Fn   *|*   A*|*S*|*D*|*F  *|*G *|* H *|*J *|*   K*|*L *|* Bk*|
 * |*LSFT *|*   Z*|*X*|*C*|*V  *|*B *|* N *|*M *|*   ,*|*. *|*  ?*|
 * |*;    *|*   +*|*-*|*'*|*SPC*|    |*SPC*|*Su*|*RSFT*|*Fn*|*Ret*|
 * |*LCTRL*|*LALT*|                                    |*Up*|
 *                                                |*Le*|    |*Ri*|
 *                                                     |*Dn*|*/
const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
    /* Base */
    [_BASE] = LAYOUT(
        KC_ESC,   KC_1,     KC_2,     KC_3,     KC_4,     KC_5,     KC_6,     KC_7,     KC_8,      KC_9,  KC_0,
        KC_TAB,   KC_Q,     KC_W,     KC_E,     KC_R,     KC_T,     KC_Y,     KC_U,     KC_I,     KC_O,   KC_P,
        MO(_FN),  KC_A,     KC_S,     KC_D,     KC_F,     KC_G,     KC_H,     KC_J,     KC_K,     KC_L,   KC_BSPC,
        KC_LSFT,  KC_Z,     KC_X,     KC_C,     KC_V,     KC_B,     KC_N,     KC_M,     KC_COMMA, KC_DOT, KC_SLASH,
        KC_SCOLON,KC_EQUAL, KC_MINUS, KC_QUOTE, KC_SPC,             KC_SPC,   KC_RGUI,  KC_RSFT,  MO(_FN),KC_ENTER,
        KC_LCTRL, KC_LALT,                                                                        KC_UP,
                                                                                        KC_LEFT,          KC_RIGHT,
                                                                                                  KC_DOWN
    ),
    [_FN] = LAYOUT(
        KC_NO,    KC_F1,    KC_F2,    KC_F3,    KC_F4,    KC_F5,    KC_F6,    KC_F7,    KC_F8,    KC_F9,  KC_F10,
        KC_NO,    KC_NO,    KC_NO,    KC_NO,    KC_NO,    KC_NO,    KC_NO,    KC_NO,    KC_NO,    KC_NO,  KC_NO,
        MO(_FN),  KC_NO,    KC_NO,    KC_NO,    KC_NO,    KC_NO,    KC_NO,    KC_NO,    KC_NO,    KC_NO,  KC_DELETE,
        KC_NO,    KC_NO,    KC_NO,    KC_NO,    KC_NO,    KC_NO,    KC_NO,    KC_NO,    KC_NO,    KC_NO,  KC_BSLASH,
        KC_NO,    KC_NO,    KC_NO,    KC_NO,    RESET,              RESET,    KC_NO,    KC_NO,    MO(_FN),KC_NO,
        KC_AUDIO_VOL_DOWN, KC_AUDIO_VOL_UP,                                                       KC_PGUP,
                                                                                        KC_HOME,          KC_END,
                                                                                                  KC_PGDOWN
    )
};

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
    switch (keycode) {
        case QMKBEST:
            if (record->event.pressed) {
                // when keycode QMKBEST is pressed
                SEND_STRING("QMK is the best thing ever!");
            } else {
                // when keycode QMKBEST is released
            }
            break;
        case QMKURL:
            if (record->event.pressed) {
                // when keycode QMKURL is pressed
                SEND_STRING("https://qmk.fm/\n");
            } else {
                // when keycode QMKURL is released
            }
            break;
    }
    return true;
}


void matrix_init_user(void)
{
    debug_enable = true;
    debug_matrix = true;
}
/*
void matrix_scan_user(void) {

}

bool led_update_user(led_t led_state) {
    return true;
}
*/
