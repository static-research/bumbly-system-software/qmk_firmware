o=# bumblydevicekbd

![bumblydevicekbd](imgur.com image replace me!)

This keyboard is designed to be internal to the Bumbly mobile communication device, however it is designed to be used via usb.

* Keyboard Maintainer: [core2idiot](https://github.com/yourusername)
* Hardware Supported: The PCBs, controllers supported
* Hardware Availability: Links to where you can find this hardware

Make example for this keyboard (after setting up your build environment):

    make bumblydevicekbd:default

See the [build environment setup](https://docs.qmk.fm/#/getting_started_build_tools) and the [make instructions](https://docs.qmk.fm/#/getting_started_make_guide) for more information. Brand new to QMK? Start with our [Complete Newbs Guide](https://docs.qmk.fm/#/newbs).
